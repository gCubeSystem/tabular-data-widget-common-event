/**
 * 
 */
package org.gcube.portlets.user.td.widgetcommonevent.client.type;

/**
 * 
 * @author Giancarlo Panichi 
 * 
 *
 */
public enum DataViewActiveType {
	NEWACTIVE,
	ACTIVEAFTERCLOSE,
	CLOSE;
}
